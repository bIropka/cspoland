<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 */

?>

</main>

<footer>

    <div class="container">

        <div class="row">

            <div class="col-12 col-md-6 col-lg-3">

                <h3><?php ?></h3>

                <ul>
                    <li>
				        <?php
                            $footer_item_image = '';
                            if($footer_item_image) {
                                echo '<img src="<?php ?>" alt="contact data">';
                            }
				        ?>
                        <a href="<?php ?>"><?php ?></a>
                    </li>
                    <li>
				        <?php
                            $footer_item_image = '';
                            if($footer_item_image) {
                                echo '<img src="<?php ?>" alt="contact data">';
                            }
				        ?>
                        <a href="<?php ?>"><?php ?></a>
                    </li>
                </ul>

            </div>

            <div class="col-12 col-md-6 col-lg-3">

                <h3><?php ?></h3>

                <ul>
                    <li>
	                    <?php
                            $footer_item_image = '';
                            if($footer_item_image) {
                                echo '<img src="<?php ?>" alt="contact data">';
                            }
	                    ?>
                        <a href="<?php ?>"><?php ?></a>
                    </li>
                    <li>
	                    <?php
                            $footer_item_image = '';
                            if($footer_item_image) {
                                echo '<img src="<?php ?>" alt="contact data">';
                            }
	                    ?>
                        <a href="<?php ?>"><?php ?></a>
                    </li>
                </ul>

            </div>

            <div class="col-12 col-md-6 col-lg-3">

                <h3><?php ?></h3>

                <ul>
                    <li>
                        <?php
                            $footer_item_image = '';
                            if($footer_item_image) {
                                echo '<img src="<?php ?>" alt="contact data">';
                            }
                        ?>
                        <a href="<?php ?>"><?php ?></a>
                    </li>
                    <li>
	                    <?php
                            $footer_item_image = '';
                            if($footer_item_image) {
                                echo '<img src="<?php ?>" alt="contact data">';
                            }
	                    ?>
                        <a href="<?php ?>"><?php ?></a>
                    </li>
                </ul>

            </div>

            <div class="col-12 col-md-6 col-lg-3">

                <h3><?php ?></h3>

                <ul>
                    <li>
                        <?php
                            $footer_item_image = '';
                            if($footer_item_image) {
                                echo '<img src="<?php ?>" alt="contact data">';
                            }
                        ?>
                        <a href="<?php ?>"><?php ?></a>
                    </li>
                    <li>
	                    <?php
                            $footer_item_image = '';
                            if($footer_item_image) {
                                echo '<img src="<?php ?>" alt="contact data">';
                            }
	                    ?>
                        <a href="<?php ?>"><?php ?></a>
                    </li>
                </ul>

            </div>

        </div>

    </div>

    <div class="footer-bottom">

        <div class="container">

            <div class="row">

                <div class="col-12 col-md-4">
                    <div class="copyrights">
                        <?php ?>
                    </div>
                </div>

                <div class="col-12 col-md-4">
                    <div class="developer">Realizacja: <a href="#">MX Studio</a></div>
                </div>

                <div class="col-12 col-md-4">
                    <div class="cookies">
                        <a href="#"><?php ?></a>
                    </div>
                </div>

            </div>

        </div>

    </div>

</footer>

</div> <!-- end of wrapper -->

<script src="<?php echo get_template_directory_uri(); ?>/assets/public/js/jquery.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/public/js/scripts.min.js"></script>

<?php wp_footer(); ?>
</body>

</html>