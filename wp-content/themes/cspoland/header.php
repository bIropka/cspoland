<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <main>
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo wp_get_document_title(); ?></title>
    <link rel="icon" href="<?php echo get_template_directory_uri();?>/favicon.ico" type="image/x-icon" />
	<?php wp_head(); ?>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/public/css/custom-styles.css">
</head>

<body <?php body_class(); ?>>

<div id="page" class="wrapper site">

    <header>

        <div class="container">

            <div class="row">

                <div class="logo">
                    <img src="<?php ?>" alt="CS Poland">
                </div>

                <div class="col-6">

                    <div class="language">
                        <div class="language-icon">
                            <img src="<?php ?>" alt="globe">
                        </div>
                        <div class="language-text"><?php ?></div>
                        <ul>
                            <li>RU</li>
                            <li>PL</li>
                            <li>EN</li>
                        </ul>
                    </div>

                </div>

                <div class="col-6">

                    <div class="phone">
                        <div class="phone-icon">
                            <img src="<?php ?>" alt="phone">
                        </div>
                        <div class="phone-text"><?php ?></div>
                    </div>

                </div>

            </div>

            <div class="row">

                <div class="burger">
                    <div class="burger-top"></div>
                    <div class="burger-center"></div>
                    <div class="burger-bottom"></div>
                </div>

                <div class="col-6">

	                <?php
	                wp_nav_menu( array(
		                'menu'            => 'menu-anchors',
		                'menu_class'      => 'menu',
		                'container'       => 'nav',
		                'container_class' => 'nav-anchors'
	                ) );
	                ?>

                </div>

                <div class="col-6">

	                <?php
	                wp_nav_menu( array(
		                'menu'            => 'menu-main',
		                'menu_class'      => 'menu',
		                'container'       => 'nav',
		                'container_class' => 'nav-main'
	                ) );
	                ?>

                </div>

            </div>

        </div>

    </header>

    <main>