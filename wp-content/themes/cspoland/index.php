<?php
/**
 * The main template file
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 */

get_header(); ?>

<div class="top">

    <div class="top-image">
        <img src="<?php the_field( 'top_image_img' ) ?>" alt="<?php the_field( 'top_image_alt' ) ?>">
    </div>

    <div class="wrapper-inner">

        <ul class="top-list">

			<?php
			$top_list = get_field( 'top_list' );
			if ( $top_list ) {
				foreach ( $top_list as $key => $value ) {
					?>
                    <li><?php echo $value['top_list_item']; ?></li>
					<?php
				}
			}
			?>

        </ul>

    </div>

</div>

<section class="services" id="services">

    <div class="wrapper-inner">

        <h2><?php if ( get_field( 'services_title' ) ) {
				the_field( 'services_title' );
			} ?></h2>

        <div class="services-list">

			<?php
			$services = get_field( 'services' );
			if ( $services ) {
				foreach ( $services as $key => $value ) {
					?>
                    <a class="services-item" href="<?php echo $value['service_url']; ?>">
                        <div class="services-title"><?php echo $value['service_title']; ?></div>
                        <div class="services-icon"><img src="<?php echo $value['service_image']; ?>"
                                                        alt="<?php echo $value['service_image_alt']; ?>"></div>
                        <div class="services-subtitle"><?php echo $value['service_subtitle']; ?></div>
                    </a>
					<?php
				}
			}
			?>

        </div>

		<?php
		$services_text = get_field( 'services_text' );
		if ( $services_text ) {
			foreach ( $services_text as $key => $value ) {
				echo $value['text'];
			}
		}
		?>

    </div>

</section>

<section class="portfolio" id="portfolio">

    <div class="wrapper-inner">

        <div class="portfolio-slider">

			<?php
			$portfolio = get_field( 'portfolio' );
			if ( $portfolio ) {
				foreach ( $portfolio as $key => $value ) {
					?>
                    <div class="slide"><img src="<?php echo $value['image']; ?>"
                                            alt="<?php echo $value['image_alt']; ?>"></div>
					<?php
				}
			}
			?>

        </div>

    </div>

</section>

<?php
get_footer();
?>
